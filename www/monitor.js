module.exports = {
	MonitorStart: function (args, successCallback, errorCallback) {
		cordova.exec(successCallback, errorCallback, "image-monitor", "MonitorStart", [args]);
	},
	MonitorImagePop: function (args, successCallback, errorCallback) {
		cordova.exec(successCallback, errorCallback, "image-monitor", "MonitorImagePop", [args]);
	}
};
