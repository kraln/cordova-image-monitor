package ai.gesundheit.plugins;

/**
 * A media monitor plugin for Cordova/Phonegap
 */

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;


/***
 * Concept:
 *      MonitorStart (idempotent):
 *          Create an Observer for the image media store
 *          When the observer gets a new picture,
 *              toss it into a temporary file
 *              resize it to a thumbnail
 *              base64 the thumbnail
 *              add the thumbnail and the file path to the pending list
 *      MonitorImagePop:
 *          pop item from the pending list
 */

public class MediaMonitor extends CordovaPlugin {
    public static String TAG = "MediaMonitor";
    private Boolean started = false;
    private CallbackContext callbackContext;
    private JSONObject params;
    private Context context;
    private Queue<String> q;

    class MediaObserver extends ContentObserver {
        private Queue<String> myQueue;
	private String lastURL = "";

        public MediaObserver(Handler handler) {
            super(handler);
        }

        public void setQueue(Queue<String> towhat)
        {
            myQueue = towhat;
        }
        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            Log.i(TAG, "Change event! (" + selfChange + "), [" + uri.toString() + "]");
            if(lastURL.equals(uri.toString()))
            {
                return;
            }

            lastURL = uri.toString();
            try {
                Bitmap bm = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri));
                Bitmap scaled = Bitmap.createScaledBitmap(bm, 200, 200, true);
                ByteArrayOutputStream BAOS = new ByteArrayOutputStream();
                scaled.compress(Bitmap.CompressFormat.JPEG, 100, BAOS);
                String encoded_scaled = Base64.encodeToString(BAOS.toByteArray(), Base64.NO_WRAP);
                Random r = new Random();

                File file = null;
                try {
                    file = File.createTempFile("image", ".jpg", context.getExternalCacheDir());
                    FileOutputStream filecon = new FileOutputStream(file);
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, filecon);
                    myQueue.add("[{ \"thumb\": \"" + encoded_scaled + "\", \"path\": \"" + file.getPath() + "\" }]");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    Handler handler;
    MediaObserver observer;

    public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;
        context = this.cordova.getActivity().getApplicationContext();
        if (action.equals("MonitorStart"))
        {
            if (!started)
            {
                started = true;
                handler = new Handler();
                observer = new MediaObserver(handler);
                q = new PriorityQueue<String>();
                observer.setQueue(q);
                context.getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true, observer);
            }

            callbackContext.success();
            return true;
        }

        if (action.equals("MonitorImagePop"))
        {
            String obj = q.poll();
            JSONArray res;
            if(obj != null)
            {
                res = new JSONArray(obj);
            } else {
                res = new JSONArray();
            }

            callbackContext.success(res);
            return true;
        }
        return false;
    }

    /* cleanup observer registration */
    @Override public void onDestroy () {
        if(observer != null) {
            context.getContentResolver().unregisterContentObserver(observer);
        }

        super.onDestroy ();
    };
}
